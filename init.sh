echo "apt update, upgrade, and install"
apt update
apt upgrade -y
apt install -y curl
apt install -y unzip

# echo "Install the IBM Cloud CLI"
# curl -fsSL https://clis.cloud.ibm.com/install/linux | sh

echo "Download ibmcloud .tar.gz"
cd /vol && curl -k -O https://download.clis.cloud.ibm.com/ibm-cloud-cli/1.2.3/IBM_Cloud_CLI_1.2.3_amd64.tar.gz
cd /vol && tar xzvf IBM_Cloud_CLI_1.2.3_amd64.tar.gz
cd /vol/Bluemix_CLI && ./install


echo "Install cf plugin"
ibmcloud cf install


echo "Container Registry Plugin"
ibmcloud plugin install container-registry
ibmcloud plugin install container-service
ibmcloud plugin install code-engine

# https://cloud.ibm.com/docs/cloud-foundry-public?topic=cloud-foundry-public-endpoints
echo "US-SOUTH api.us-south.cf.cloud.ibm.com"

ibmcloud login --sso


ibmcloud target --cf-api api.us-south.cf.cloud.ibm.com -o "willem.hendriks@nl.ibm.com"  -s dev

ibmcloud target -g default

echo "ibmcloud cr build --tag us.icr.io/willemh/dofa:1"
echo "ibmcloud cf push customimage23453245002 --docker-image=us.icr.io/willemh/dofa:2 --docker-username iamapikey"
#
# ibmcloud cf push customimage23453245002 --docker-image=us.icr.io/willemh/dofa:2 --docker-username iamapikey
#

#
# Download & install terraform
#

cd && mkdir terraform
cd /root/terraform && curl -O https://releases.hashicorp.com/terraform/0.13.4/terraform_0.13.4_linux_amd64.zip
cd /root/terraform && unzip terraform_*.zip
cd && rm *.zip
export PATH=$PATH:$HOME/terraform
echo export PATH=$PATH:$HOME/terraform >> /root/.bashrc
terraform -v

#
# Download IBM TerraForm Plugin

mkdir $HOME/.terraform.d/plugins
cd $HOME/.terraform.d/plugins && curl -L -O https://github.com/IBM-Cloud/terraform-provider-ibm/releases/download/v1.13.1/terraform-provider-ibm_1.13.1_linux_amd64.zip
cd $HOME/.terraform.d/plugins && unzip terraform-provider-ibm_*.zip
cd $HOME/.terraform.d/plugins && rm *.zip

#